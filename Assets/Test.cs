﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Test : MonoBehaviour {
    public GameObject but1;
    public GameObject but2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void turnRed()
    {
        if (but1.GetComponent<Image>().color == Color.red)
        {
            but1.GetComponent<Image>().color = Color.white;

        }
        else
        {
            but1.GetComponent<Image>().color = Color.red;

        }
    }
    public void turnGreen()
    {
        if (but2.GetComponent<Image>().color == Color.green)
        {
            but2.GetComponent<Image>().color = Color.white;

        }
        else
        {
            but2.GetComponent<Image>().color = Color.green;

        }
    }
}
